﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRules : MonoBehaviour
{
    // Indica a vez do jogador (1 ou 2)
    public static int playerTurn;
    // Armazena os objetos do jogo usados para indicar Oásis
    public static GameObject[] oases;
    // Indicadores do estado do jogo e flags que indicam se o jogador da vez está realizando
    // alguma ação mais alongada
    public static bool isReady, isOver = false, waitingToCollect = false;
    // Lista de campos do jogo
    public static GameObject[] fields = new GameObject[12];
    public static int selectedFieldIndex = -1, restToPlayPedras = 0;
    // Lista com os índices dos campos alterados nesta rodada
    // (não conta o campo onde iniciou-se a jogada)
    public static List<int> roundFieldIndex = new List<int>();
    public static bool is2ndPlayerAuto = true;
    public static float currentRound = 1f;
    // Indica se há um campo que cada um dos jogadores precisam jogar
    public static int[] requiringAttFields = new int[] { -1, -1 };
    public static int[] emptyFields = new int[] { 0, 0 };
    public static int[] toDisputePoints = new int[] { 0, 0 };
    public static int pointsPerPlayer;
    public static int[,] pointsPerPhase = new int[2, 3];
    public static int phase = 1;
    // Variáveis de personalização dos jogadores (nome, personagens etc.)
    public static string[] playersNames = new string[] { "1º jogador", "2º jogador" };
    public static int[] selectedCharsIdx = new int[] { -1, -1 };

    // CONSTANTES
    public static int MAX_PEDRAS_PER_FIELD = 11;
}
