﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerSetterScript : MonoBehaviour
{
    public InputField timerInputField;
    public Text timerStatusText;
    private string PLAYER_PREF_NAME_TO_TIMER = "_mancbr_timer_secs";
    void Start()
    {
        int secs = 0;
        if (PlayerPrefs.HasKey(PLAYER_PREF_NAME_TO_TIMER))
        {
            secs = PlayerPrefs.GetInt(PLAYER_PREF_NAME_TO_TIMER);
        }
        this.timerInputField.text = secs.ToString();
        this.SetStatusText(ref secs);
    }
    void SetStatusText(ref int s)
    {
        if (s >= 5)
        {
            this.timerStatusText.text = "HABILITADO!";
            this.timerStatusText.color = new Color(0.35f, 0.78f, 0.16f, 1);
        }
        else
        {
            s = 0;
            this.timerInputField.text = s.ToString();
            this.timerStatusText.text = "DESABILITADO!";
            this.timerStatusText.color = new Color(0.77f, 0.02f, 0.02f, 1);
        }
    }
    public void SetGameTimer(string secs)
    {
        int iSec;
        int.TryParse(secs, out iSec);
        this.SetStatusText(ref iSec);
        if (iSec == 0 && secs != "0")
        {
            this.timerInputField.text = "0";
        }
        PlayerPrefs.SetInt(PLAYER_PREF_NAME_TO_TIMER, iSec);
    }
}
