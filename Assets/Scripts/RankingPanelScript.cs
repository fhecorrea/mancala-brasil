﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingPanelScript : MonoBehaviour
{
    public GameObject createdRowsRepository, rankRowModel;
    private const char RANK_LOCAL_MODE = 'l', RANK_GLOBAL_MODE = 'g';
    public void ToggleRankPanel()
    {
        Debug.Log((this.gameObject.activeSelf ? "Fechando" : "Abrindo") + " painel rank!");
        this.gameObject.SetActive(!this.gameObject.activeSelf);
        if (this.gameObject.activeSelf)
        {
            this.LoadRankData(RANK_LOCAL_MODE);
        }
        else
        {
            this.ClearCurrentRankPresentation();
        }
    }
    private void ClearCurrentRankPresentation()
    {
        if (this.createdRowsRepository.transform.childCount > 0)
        {
            for (int cIdx = (this.createdRowsRepository.transform.childCount - 1); cIdx >= 0; cIdx -= 1)
            {
                Destroy(this.createdRowsRepository.transform.GetChild(cIdx).gameObject);
            }
        }
    }
    private void LoadRankData(char mode)
    {
        GameObject row;
        Color colorPlayerRow;
        FontStyle fontStyle = FontStyle.Normal;
        string playerName;
        int playerPoints;

        this.ClearCurrentRankPresentation();

        for (int playerIdx = 0; playerIdx < 10; playerIdx += 1)
        {
            if (!PlayerPrefs.HasKey("_mancbr_rank_" + (playerIdx + 1).ToString() + "_player_name") || !PlayerPrefs.HasKey("_mancbr_rank_" + (playerIdx + 1).ToString() + "_player_points"))
            {
                break;
            }
            row = Instantiate(rankRowModel);
            fontStyle = FontStyle.Normal;
            playerName = PlayerPrefs.GetString("_mancbr_rank_" + (playerIdx + 1).ToString() + "_player_name");
            playerPoints = PlayerPrefs.GetInt("_mancbr_rank_" + (playerIdx + 1).ToString() + "_player_points");

            switch (playerIdx)
            {
                case 0:
                    colorPlayerRow = new Color(0.85f, 0.65f, 0.13f, 1); // goldenrod
                    fontStyle = FontStyle.Bold;
                    break;
                case 1:
                    colorPlayerRow = new Color(0.75f, 0.76f, 0.76f, 1);
                    break;
                case 2:
                    colorPlayerRow = new Color(0.65f, 0.44f, 0.39f, 1);
                    break;
                default:
                    colorPlayerRow = new Color(0.6f, 0.6f, 0.6f, 1);
                    break;
            }
            row.name = "row #" + (playerIdx + 1).ToString();
            row.transform.GetChild(0).name = "PosRank" + (playerIdx + 1).ToString() + "Text";
            row.transform.GetChild(0).GetComponent<Text>().text = "#" + (playerIdx + 1).ToString();
            row.transform.GetChild(0).GetComponent<Text>().color = colorPlayerRow;
            row.transform.GetChild(0).GetComponent<Text>().fontStyle = fontStyle;
            row.transform.GetChild(1).name = "PlayerName" + (playerIdx + 1).ToString() + "Text";
            row.transform.GetChild(1).GetComponent<Text>().text = playerName;
            row.transform.GetChild(1).GetComponent<Text>().color = colorPlayerRow;
            row.transform.GetChild(1).GetComponent<Text>().fontStyle = fontStyle;
            row.transform.GetChild(2).name = "GamePlayer" + (playerIdx + 1).ToString() + "PointsText";
            row.transform.GetChild(2).GetComponent<Text>().text = playerPoints.ToString(); //(10 - playerIdx).ToString();
            row.transform.GetChild(2).GetComponent<Text>().color = colorPlayerRow;
            row.transform.GetChild(2).GetComponent<Text>().fontStyle = fontStyle;
            row.transform.GetChild(3).name = "TotalPlayer" + (playerIdx + 1).ToString() + "PointsText";
            row.transform.GetChild(3).GetComponent<Text>().text = "--";//((10 - playerIdx) * 1000).ToString();
            row.transform.GetChild(3).GetComponent<Text>().color = colorPlayerRow;
            row.transform.GetChild(3).GetComponent<Text>().fontStyle = fontStyle;
            row.transform.SetParent(this.createdRowsRepository.transform, false);
            row.transform.position = new Vector2(row.transform.position.x, row.transform.position.y - (1.1f * playerIdx));
            row.SetActive(true);
        }
    }
    /**
     * Verifica se o jogador obteve pontuação equivalente a uma posição no rank.
     * Se for o caso, retorna a posição no rank ou -1 caso o jogador não tenha tido pontuação
     * suficiente para estar entre os 10 primeiros.
     */
    public int HasRankLevel(int points)
    {
        int rIdx = 0;
        bool hasRankLevel = false;
        do
        {
            int p = PlayerPrefs.GetInt("_mancbr_rank_" + (rIdx + 1).ToString() + "_player_points");
            if (p < 1 || points > p)
            {
                hasRankLevel = true;
            }
            rIdx += 1;
        }
        while (hasRankLevel == false && rIdx < 10);
        if (!hasRankLevel)
        {
            return -1;
        }
        return rIdx;
    }
    public void RegisterRank(int pos, string playerName, int playerPoints)
    {
        List<int> posPrevPlayerPoints = new List<int>();
        List<string> posPrevPlayerName = new List<string>();
        bool ok = false;
        for (int posIdx = pos; posIdx <= 10; posIdx += 1)
        {
            if (PlayerPrefs.HasKey("_mancbr_rank_" + posIdx.ToString() + "_player_points") && PlayerPrefs.HasKey("_mancbr_rank_" + posIdx.ToString() + "_player_name"))
            {
                posPrevPlayerPoints.Add(PlayerPrefs.GetInt("_mancbr_rank_" + posIdx.ToString() + "_player_points"));
                posPrevPlayerName.Add(PlayerPrefs.GetString("_mancbr_rank_" + posIdx.ToString() + "_player_name"));
            }
            else
            {
                posPrevPlayerName.Add(null);
                posPrevPlayerPoints.Add(-1);
            }
            if (!ok)
            {
                PlayerPrefs.SetInt("_mancbr_rank_" + posIdx.ToString() + "_player_points", playerPoints);
                PlayerPrefs.SetString("_mancbr_rank_" + posIdx.ToString() + "_player_name", playerName);
                ok = true;
                continue;
            }
            if (posPrevPlayerName[0] != null && posPrevPlayerPoints[0] > 1)
            {
                PlayerPrefs.SetString("_mancbr_rank_" + posIdx.ToString() + "_player_name", posPrevPlayerName[0]);
                posPrevPlayerName.RemoveAt(0);
                PlayerPrefs.SetInt("_mancbr_rank_" + posIdx.ToString() + "_player_points", posPrevPlayerPoints[0]);
                posPrevPlayerPoints.RemoveAt(0);
            }
            else
            {
                break;
            }
        }
    }
}
