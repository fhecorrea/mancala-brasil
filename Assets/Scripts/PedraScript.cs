﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class PedraScript : MonoBehaviour
{

    public int value;
    private AudioSource audioSource;
    private Animator pAnim;
    private GameObject field;
    private AudioClip[] defaultSFXs, metalSFXs, forestSFXs, onManyCoinsSFXs, onManyStonesSFXs;
    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        pAnim = GetComponent<Animator>();
    }

    /**
     * Executa a animação de moção suavizada para uma determinada casa
     */
    public void MoveTo(GameObject destObj)
    {
        // Na impossibilidade de mexer no objeto "Pedra" no momento que ela
        // está sendo colocada no tabuleiro, então resolveu-se mexer na origem,
        // ou seja, para dar o ar de aleatoriedade no posicionamento das peças,
        // optou-se por alterar a posição de saída das peças.
        //this.gameObject.transform.position = new Vector2(0f, -7.5f);
        float destX, destY;
        if (destObj.GetComponent<FieldScript>().isOasis)
        {
            //destX = Random.Range(destObj.transform.position.x - 0.005f, destObj.transform.position.x + .005f);
            destX = UnityEngine.Random.Range(-.15f, .15f);
            destY = UnityEngine.Random.Range(-8.7f, -6.3f);
        }
        else
        {
            destX = UnityEngine.Random.Range(-0.35f, .35f);
            destY = UnityEngine.Random.Range(-7.7f, -7.3f);
        }
        if (GameRules.playerTurn == 2)
            destY *= -1; // inverte para vir de cima
        this.gameObject.transform.position = new Vector2(destX, destY);
        string animName = "from_" + (GameRules.playerTurn == 1 ? "bottom" : "top") + "_to_" + destObj.name;
        //Debug.Log("To execute '" + animName + "'...");
        pAnim.Play(animName);
        destObj.GetComponent<FieldScript>().pedras.Add(this.gameObject);
        //if (destObj.GetComponent<FieldScript>().pedras.Count < 1)
        //{
        int idx = (int)System.Math.Floor(UnityEngine.Random.Range(0f, 1.99f));
        if (GameRules.phase == 1)
            this.audioSource.clip = this.forestSFXs[idx];
        else if (GameRules.phase == 2)
            this.audioSource.clip = this.metalSFXs[idx];
        else
            this.audioSource.clip = this.defaultSFXs[idx];
        //}
        //else
        //{
        // TODO: Verificar qual tipo de pedra se faz maioria no campo
        //    this.audioSource.clip = this.onManyStonesSFXs[(int)System.Math.Floor(UnityEngine.Random.Range(0f, 1.99f))];
        //}
        this.field = destObj;
        //audioSource.clip = defaultSfx
    }
    /**
     * Ao mover a pedra, decrescenta do contador
     */
    public void AfterMovePedra()
    {
        GameRules.restToPlayPedras -= 1;
        if (PlayerPrefs.GetInt("_mancbr_enabled_sfx") > 0)
            audioSource.Play();
        this.field.GetComponent<FieldScript>().CountPedras();
        this.field = null;
    }

    /* SETTERs */
    public void SetDefaultSFXs(AudioClip[] audios)
    {
        this.defaultSFXs = audios;
    }
    public void SetMetalSFXs(AudioClip[] audios)
    {
        this.metalSFXs = audios;
    }
    public void SetForestSFXs(AudioClip[] audios)
    {
        this.forestSFXs = audios;
    }
    public void SetOnManyCoinsSFXs(AudioClip[] audios)
    {
        this.onManyCoinsSFXs = audios;
    }
    public void SetOnManyStonesSFXs(AudioClip[] audios)
    {
        this.onManyStonesSFXs = audios;
    }
}
