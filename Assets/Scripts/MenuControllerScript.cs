﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuControllerScript : GameSettings
{
    public GameObject loadingPanel, menuPanel, rulesPanel, settingsPanel, matchSettingsPanel;
    // Gerencia do painel de configurações da partida
    public Button goBackBtn, goToNextStepBtn;
    public Color deltaColor, infinitColor, piColor, vetorColor;
    public GameObject playersNumSubPanel, playerNameSubPanel, onePlayerBtn, twoPlayersBtn, rulesMenuLeftTable, rulesMenuRightTable, creditsPanel;
    public GameObject[] charSelectorBtn = new GameObject[4];
    public GameObject[] manualTexts = new GameObject[7];
    public InputField playerNameInput;
    public Text playerNameSubPanelTitleText, playerNameInputPlaceholderText, playerNameInputLabelText, resumePlayerText;
    public string gameScene;
    private int playerIdx = 0, is2ndPlayerAuto = 0;
    private string[] playersName = new string[2];
    private int[] playersCharIdx = new int[2] { -1, -1 };
    private Animator resumePlayerTextAnim;

    // Start is called before the first frame update
    void Start()
    {
        loadingPanel.SetActive(false);
        resumePlayerTextAnim = resumePlayerText.GetComponent<Animator>();
        onePlayerBtn.GetComponent<Image>().color = new Color(.27f, .44f, 1f, 1f);
        twoPlayersBtn.GetComponent<Image>().color = new Color(1f, .26f, .26f, 1f);
        this.isGeneralSettings = true;
    }

    // Update is called once per frame
    void Update()
    {
        ReloadAudioState();
        if (this.playerNameSubPanel.activeSelf)
        {
            if (this.playersCharIdx[this.playerIdx] == -1 && (this.playersName[this.playerIdx] == null || this.playersName[this.playerIdx].Length < 2))
            {
                this.resumePlayerText.text = "< sem nome e sem personagem selecionado >";
                this.resumePlayerText.color = Color.red;
            }
            else if (this.playersCharIdx[this.playerIdx] >= 0 && this.playersName[this.playerIdx] != null && this.playersName[this.playerIdx].Length >= 2)
            {
                this.resumePlayerText.color = Color.green;
            }
            else
            {
                this.resumePlayerText.color = Color.yellow;
            }
            this.resumePlayerText.text = "Jogador " + (this.playerIdx + 1).ToString() + ": " + (this.playersName[this.playerIdx] != null && this.playersName[this.playerIdx].Length >= 2 ? this.playersName[this.playerIdx] : "<sem nome>") + " - " + (this.playersCharIdx[this.playerIdx] == 0 ? "personagem Delta" : (this.playersCharIdx[this.playerIdx] == 1 ? "personagem Infinit" : (this.playersCharIdx[this.playerIdx] == 2 ? "personagem Pi" : this.playersCharIdx[this.playerIdx] == 3 ? "personagem Vector" : "nenhuma personagem"))) + " selecionada";
        }
    }

    public void Play()
    {
        loadingPanel.SetActive(true);
        SceneManager.LoadScene(this.gameScene);
    }

    public void Quit()
    {
        Application.Quit();
    }
    public void ToggleRulesPanel()
    {
        this.ToggleMenuPanel(this.rulesPanel);
        if (this.rulesPanel)
        {
            this.manualTexts[0].SetActive(true);
            this.rulesMenuLeftTable.SetActive(false);
        }
        else
        {
            foreach (GameObject mTxt in manualTexts)
            {
                mTxt.SetActive(false);
            }
        }
    }
    public void ToggleSettingsPanel()
    {
        this.ToggleMenuPanel(this.settingsPanel);
        if (this.settingsPanel.activeSelf)
            this.LoadPresetSettings();
    }
    public void ToggleCreditsPanel()
    {
        this.ToggleMenuPanel(this.creditsPanel);
    }
    public void GoToNextPageManual()
    {
        for (int p = 0; p < this.manualTexts.Length; p += 1)
        {
            if (this.manualTexts[p].activeSelf)
            {
                this.manualTexts[p].SetActive(!this.manualTexts[p].activeSelf);
                this.manualTexts[p + 1].SetActive(!this.manualTexts[p].activeSelf);
                if (p == (this.manualTexts.Length - 2))
                {
                    this.rulesMenuRightTable.SetActive(false);
                }
                if ((p + 1) > 0 && !this.rulesMenuLeftTable.activeSelf)
                {
                    this.rulesMenuLeftTable.SetActive(true);
                }
                break;
            }
        }
    }
    public void GoToPrevPageManual()
    {
        for (int p = (this.manualTexts.Length - 1); p >= 0; p -= 1)
        {
            if (this.manualTexts[p].activeSelf)
            {
                this.manualTexts[p].SetActive(!this.manualTexts[p].activeSelf);
                this.manualTexts[p - 1].SetActive(!this.manualTexts[p].activeSelf);
                if (p == 1)
                {
                    this.rulesMenuLeftTable.SetActive(false);
                }
                if (!this.rulesMenuRightTable.activeSelf)
                {
                    this.rulesMenuRightTable.SetActive(true);
                }
                break;
            }
        }
    }
    // Match settngs -----
    public void LoadMatchSettingsPanel()
    {
        this.ToggleMenuPanel(matchSettingsPanel);
        playersNumSubPanel.SetActive(true);
        playerNameSubPanel.SetActive(false);
        this.UpdateBtnState();
        this.resumePlayerText.text = "< selecione se quer jogar contra um computador ou um amigo >";
        this.resumePlayerTextAnim.SetInteger("current_pass", 1);
    }
    public void BackToPrev()
    {
        if (this.playerIdx == 0 && this.playersNumSubPanel.activeSelf)
        {
            this.is2ndPlayerAuto = this.playerIdx = 0;
            this.playersCharIdx = new int[] { -1, -1 };
            this.playersName = new string[2];
            ToggleMenuPanel(this.matchSettingsPanel);
        }
        else if (this.playerNameSubPanel.activeSelf && playerIdx == 1)
        {
            playerIdx -= 1;
        }
        else if (this.playerNameSubPanel.activeSelf && playerIdx == 0)
        {
            playerNameSubPanel.SetActive(!this.playerNameSubPanel.activeSelf);
            playersNumSubPanel.SetActive(!this.playerNameSubPanel.activeSelf);
            this.resumePlayerTextAnim.SetInteger("current_pass", 1);
        }
        UpdateBtnState();
    }
    public void Advance()
    {
        if (this.playersNumSubPanel.activeSelf)
        {
            if (this.playersNumSubPanel.activeSelf)
            {
                this.playersNumSubPanel.SetActive(!this.playersNumSubPanel.activeSelf);
                this.playerNameSubPanel.SetActive(!this.playersNumSubPanel.activeSelf);
                this.resumePlayerTextAnim.SetInteger("current_pass", 2);
            }
            this.playerNameSubPanelTitleText.text = "Nome e personagem - " + (playerIdx == 0 ? "primeiro" : "segundo");
            this.playerNameInputPlaceholderText.text = "Digite aqui o nome/apelido ...";
            this.playerNameInputLabelText.text = "Nome do " + (playerIdx + 1).ToString() + "º jogador:";
            if (this.playersName[this.playerIdx] != null && this.playersName[this.playerIdx].Length >= 2 && this.playersCharIdx[this.playerIdx] != -1)
                this.goToNextStepBtn.GetComponent<Button>().interactable = true;
            else
                this.goToNextStepBtn.GetComponent<Button>().interactable = false;
            if (this.playersName[this.playerIdx] != null && this.playersName[this.playerIdx].Length >= 2)
            {
                this.playerNameInput.text = this.playersName[this.playerIdx];
                //this.goToNextStepBtn.GetComponent<Button>().interactable = true;
            }
            if (this.playersCharIdx[this.playerIdx] != -1)
            {
                if (this.playersCharIdx[this.playerIdx] == 0)
                {
                    SelectDeltaChar();
                }
                else if (this.playersCharIdx[this.playerIdx] == 1)
                {
                    SelectInfinitChar();
                }
                else if (this.playersCharIdx[this.playerIdx] == 2)
                {
                    SelectPiChar();
                }
                else if (this.playersCharIdx[this.playerIdx] == 3)
                {
                    SelectVetorChar();
                }
                //this.goToNextStepBtn.GetComponent<Button>().interactable = true;
            }
            this.UpdateBtnState();
        }
        else if (this.playerNameSubPanel.activeSelf)
        {
            if (this.is2ndPlayerAuto == -1 && this.playerIdx == 0 || playerIdx == 1)
            {
                GameRules.playersNames = this.playersName;
                GameRules.selectedCharsIdx = this.playersCharIdx;
                GameRules.is2ndPlayerAuto = this.is2ndPlayerAuto == -1;
                this.ToggleMenuPanel(this.matchSettingsPanel);
                Play();
            }
            else
            {
                playerIdx += 1;
                this.playerNameInput.Select();
                this.playerNameInput.text = "";
                for (int bIdx = 0; bIdx < this.charSelectorBtn.Length; bIdx += 1)
                {
                    this.charSelectorBtn[bIdx].GetComponent<Image>().color = Color.white;
                }
                this.playerNameSubPanelTitleText.text = "Nome e personagem - " + (playerIdx == 0 ? "primeiro" : "segundo");
                this.playerNameInputPlaceholderText.text = "Digite aqui o nome/apelido ...";
                this.playerNameInputLabelText.text = "Nome do " + (playerIdx + 1).ToString() + "º jogador:";
            }
        }
    }
    /**
     * Seletores de quantidade de jogadores (1 ou 2, por enquanto)
     */
    public void SetOnePlayer()
    {
        this.SelectNumChar(-1);
    }
    public void SetTwoPlayers()
    {
        this.SelectNumChar(1);
    }
    public void SetPlayerName(string name)
    {
        this.playersName[this.playerIdx] = name;
        if (name.Length >= 2 && this.playersCharIdx[this.playerIdx] != -1)
        {
            this.goToNextStepBtn.interactable = true;
        }
        else
        {
            this.goToNextStepBtn.interactable = false;
        }
    }
    /**
     * Seletores da personagem do jogador;
     */
    public void SelectDeltaChar()
    {
        this.SelectCharByIndex(0);
    }
    public void SelectInfinitChar()
    {
        this.SelectCharByIndex(1);
    }
    public void SelectPiChar()
    {
        this.SelectCharByIndex(2);
    }
    public void SelectVetorChar()
    {
        this.SelectCharByIndex(3);
    }
    private void UpdateBtnState()
    {
        // Se estiver configurando o ultimo jogador...
        if (((this.is2ndPlayerAuto == -1 && this.playerIdx == 0) || (this.is2ndPlayerAuto == 1 && this.playerIdx == 1)) && this.playerNameSubPanel.activeSelf)
        {
            this.goToNextStepBtn.gameObject.transform.GetChild(0).GetComponent<Text>().text = "Jogar!";
        }
        else
        {
            this.goToNextStepBtn.gameObject.transform.GetChild(0).GetComponent<Text>().text = "Avançar >";
        }
        // Se estiver na primeira 'página'...
        if (this.playerIdx == 0 && this.playersNumSubPanel.activeSelf)
        {
            this.goBackBtn.gameObject.transform.GetChild(0).GetComponent<Text>().text = "< Sair";
        }
        else
        {
            this.goBackBtn.gameObject.transform.GetChild(0).GetComponent<Text>().text = "< Voltar";
        }
        // Desabilita o botão de avanço caso haja dados não informados
        if (this.is2ndPlayerAuto == 0 || (this.is2ndPlayerAuto == 1 && ((string.IsNullOrWhiteSpace(this.playersName[0]) || string.IsNullOrWhiteSpace(this.playersName[1])) || (this.playersCharIdx[0] == -1 || this.playersCharIdx[1] == -1))) || (this.is2ndPlayerAuto == -1 && string.IsNullOrWhiteSpace(this.playersName[0])))
        {
            this.goToNextStepBtn.interactable = false;
        }
        else
        {
            this.goToNextStepBtn.interactable = true;
        }
    }
    private void SelectCharByIndex(int idx)
    {
        for (int bIdx = 0; bIdx < this.charSelectorBtn.Length; bIdx += 1)
        {
            this.charSelectorBtn[bIdx].GetComponent<Image>().color = Color.white;
        }
        if (this.playersCharIdx[this.playerIdx] != idx)
        {
            this.charSelectorBtn[idx].GetComponent<Image>().color = this.deltaColor;
            this.playersCharIdx[this.playerIdx] = idx;
            if (!string.IsNullOrWhiteSpace(this.playersName[this.playerIdx]) && this.playersName[this.playerIdx].Length >= 2)
            {
                this.goToNextStepBtn.interactable = true;
            }
        }
        else
            this.playersCharIdx[this.playerIdx] = -1;
    }
    private void SelectNumChar(int newValToAuto2ndPlayer)
    {
        Color withOnePlayerColor = new Color(.27f, .44f, 1f, 1f), withTwoPlayersColor = new Color(1f, .26f, .26f, 1f);
        if (this.is2ndPlayerAuto != newValToAuto2ndPlayer)
        {
            if (newValToAuto2ndPlayer == -1)
            {
                is2ndPlayerAuto = -1;
                this.resumePlayerText.text = "Você escolheu \"jogar sozinho contra o computador!\"";
                goToNextStepBtn.GetComponent<Button>().interactable = true;
                withTwoPlayersColor = Color.gray;
            }
            else if (newValToAuto2ndPlayer == 1)
            {
                this.is2ndPlayerAuto = 1;
                this.resumePlayerText.text = "Você escolheu \"jogar com dois jogadores!\"";
                goToNextStepBtn.GetComponent<Button>().interactable = true;
                withOnePlayerColor = Color.gray;
            }
        }
        else
        {
            this.is2ndPlayerAuto = 0;
            this.resumePlayerText.text = "< selecione se quer jogar contra um computador ou um amigo >";
            goToNextStepBtn.GetComponent<Button>().interactable = false;
        }
        onePlayerBtn.GetComponent<Image>().color = withOnePlayerColor;
        twoPlayersBtn.GetComponent<Image>().color = withTwoPlayersColor;
    }
    // -----
    private void ToggleMenuPanel(GameObject panelToToggle)
    {
        panelToToggle.SetActive(!panelToToggle.activeSelf);
        this.menuPanel.SetActive(!this.menuPanel.activeSelf);
    }
}
