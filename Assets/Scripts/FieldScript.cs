﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FieldScript : MonoBehaviour
{

    // Para cada campo, são permitidos no máximo 11 pedras
    // permitindo apenas a jogada do campo cheio
    // TODO: Procurar animar a moçao das pedras por ordem, de forma que pareçam que uma sai
    // TODO: Arrumar isso aqui quando o jogo termina e não termina (jogo CPU)...
    // Ver: https://www.geeksforgeeks.org/how-to-sort-list-in-c-sharp-set-1/
    //
    public List<GameObject> pedras = new List<GameObject>();
    public bool isOasis = false;
    public PedrasOrdererScript pedrasOrderInstance;
    public Text counter;
    private int objIdx;
    //private Transform counter;

    void Start()
    {
        string name = this.name;
        if (this.isOasis)
        {
            this.objIdx = System.Convert.ToInt32(name.Replace("oasis_", ""));
            return;
        }
        // Converte o último char do nome do campo para inteiro
        this.objIdx = System.Convert.ToInt32(name.Replace("casa_", ""));
    }

    private void MovePedra(GameObject pedra, GameObject destObj)
    {
        pedra.GetComponent<PedraScript>().MoveTo(destObj);
    }
    public void SendPedrasTo(GameObject destObj)
    {
        GameObject pedra;
        if (GameRules.is2ndPlayerAuto && GameRules.playerTurn == 2)
        {
            if (destObj.GetComponent<FieldScript>().isOasis)
            {
                pedra = this.GetMostValuedPedra();
            }
            else
            {
                pedra = this.GetLessValuedPedra();
            }
        }
        else
        {
            pedra = this.pedras[0];
        }
        this.MovePedra(pedra, destObj);
        this.pedras.Remove(pedra);
    }
    public IEnumerator StartPedrasDist()
    {
        //Debug.Log("Preparando moção de pedras do campo #" + this.objIdx.ToString() + "...");
        bool isMarked = false;
        if (pedras.Count >= 11)
        {
            isMarked = true;
        }
        GameRules.isReady = false;
        int neighIdx = this.GetNextNeighborIndex(this.objIdx); // pega e já verifica se o primeiro vizinho tem 11 peças
        GameRules.waitingToCollect = true;
        GameRules.restToPlayPedras = pedras.Count;
        while (pedras.Count > 0)
        {
            // Se os próximos campos forem oásis, deposita a bola ali
            Debug.Log(GameRules.fields[neighIdx].name + ": P#" + GameRules.playerTurn.ToString());
            if ((GameRules.fields[neighIdx].name == "casa_6" && GameRules.playerTurn == 1) || (GameRules.fields[neighIdx].name == "casa_0" && GameRules.playerTurn == 2))
            {
                SendPedrasTo(GameRules.oases[GameRules.playerTurn - 1]);
                yield return new WaitForSeconds(.5f);
            }
            if (pedras.Count > 0)
            {
                SendPedrasTo(GameRules.fields[neighIdx]);
                GameRules.roundFieldIndex.Add(neighIdx);
                neighIdx = this.GetNextNeighborIndex(neighIdx);
            }
            Debug.Log("Distribuida uma pedra. Pedras restantes: " + GameRules.restToPlayPedras.ToString());
            // Espera para declarar o jogo como pronto
            //System.Threading.Thread.Sleep(200);
            yield return new WaitForSeconds(.5f);
        }
        this.CountPedras();
        //Debug.Log("Set isReady to true");
        //GameRules.isReady = true;
        //GameRules.isWatingToCollect = true;
        if (isMarked)
        {
            Transform attentionMark = GameObject.Find("field_" + this.objIdx).transform.Find("attention_to_" + GameRules.fields[this.objIdx].name);
            Destroy(attentionMark.gameObject);
            GameRules.requiringAttFields[GameRules.playerTurn - 1] = -1;
        }
        GameRules.selectedFieldIndex = -1;
        //Debug.Log("... moção de pedras do campo #" + this.objIdx.ToString() + " finalizada.");
        yield return new WaitForSeconds(0.015f * (11 - neighIdx)); // XXX: Estava no fim do loop while
        GameRules.isReady = true;
    }

    public void MovePedrasForward()
    {
        if (!GameRules.isReady || this.isOasis || this.objIdx >= 0 && this.objIdx < 6 && GameRules.playerTurn != 1 || this.objIdx >= 6 && this.objIdx < 12 && GameRules.playerTurn != 2)
        {
            Debug.Log("O campo #" + this.objIdx.ToString() + " é um Oásis (" + this.isOasis.ToString() + "), o jogo não está pronto (" + GameRules.isReady.ToString() + ") ou é a vez do adversário(" + GameRules.playerTurn.ToString() + ")!");
            return;
        }
        // TODO: Corrigir mensagem abaixo mesmo em jogo do CPU, quando meu campo estiver cheio
        if ((GameRules.phase < 3 && pedras.Count > 1) || (GameRules.phase == 3 && pedras.Count > 2)) // antes era 0 (zero)
        {
            if (pedras.Count < 11)
            {
                // Verifica se há algum campo com 11 pedras no jogo. Se houver,
                // o jogo é bloqueado para o jogador jogar apenas aquele campo.
                for (int idx = (GameRules.playerTurn == 1 ? 0 : 6); idx < (GameRules.playerTurn == 1 ? 0 : 6) + 6; idx += 1)
                {
                    if (idx != this.objIdx && GameRules.fields[idx].GetComponent<FieldScript>().pedras.Count >= 11)
                    {
                        Debug.Log("Zere as pedras do campo " + GameRules.fields[idx].name + " cheio primeiro!");
                        return;
                    }
                }
            }
            GameRules.selectedFieldIndex = this.objIdx;
            // caso o campo tenha apenas uma pedra, não teria por que escolher
            // sendo assim, apenas move a pedra para o próximo campo
            if (/*pedras.Count == 1 || */ GameRules.playerTurn == 2 && GameRules.is2ndPlayerAuto)
            {
                StartCoroutine(this.StartPedrasDist());
            }
            else // se não, apresenta 
            {
                pedrasOrderInstance.ShowPedrasSelectorPanel();
            }
        }
        else
        {
            Debug.Log("Sem pedras para moção.");
        }
    }
    private int GetNextNeighborIndex(int index)
    {
        if (index >= GameRules.fields.Length - 1)
        {
            index = 0;
        }
        else
        {
            index += 1;
        }
        //Debug.Log("Idx: " + index + " / Len: " + GameRules.fields.Length);
        if (GameRules.fields[index].GetComponent<FieldScript>().pedras.Count == 11)
        {
            return this.GetNextNeighborIndex(index);
        }
        return index;
    }
    private GameObject GetLessValuedPedra()
    {
        GameObject lvpedra = null;
        foreach (GameObject p in this.pedras)
        {
            if (lvpedra == null || p.GetComponent<PedraScript>().value < lvpedra.GetComponent<PedraScript>().value)
            {
                lvpedra = p;
            }
        }
        return lvpedra;
    }
    private GameObject GetMostValuedPedra()
    {
        GameObject mvpedra = null;
        foreach (GameObject p in this.pedras)
        {
            if (mvpedra == null || p.GetComponent<PedraScript>().value > mvpedra.GetComponent<PedraScript>().value)
            {
                mvpedra = p;
            }
        }
        return mvpedra;
    }
    public void CountPedras()
    {
        if (!this.isOasis)
        {
            this.counter.text = this.pedras.Count.ToString();
        }
        else
        {
            int total = 0;
            foreach (GameObject p in this.pedras)
            {
                total += p.GetComponent<PedraScript>().value;
            }
            //Debug.Log("O" + objIdx + ": pos[" + (GameRules.playerTurn - 1).ToString() + ", " + (GameRules.phase - 1).ToString() + "]");
            GameRules.pointsPerPhase[(GameRules.playerTurn - 1), (GameRules.phase - 1)] = total;
            this.counter.text = total.ToString();
        }
    }
    private IEnumerator StartPedrasMoving()
    {
        Debug.Log("Preparando moção de pedras do campo #" + this.objIdx.ToString() + " para " + GameRules.oases[GameRules.playerTurn - 1].name);
        GameRules.isReady = false;
        while (pedras.Count > 0)
        {
            //int p = pedras.Count - 1;
            SendPedrasTo(GameRules.oases[GameRules.playerTurn - 1]);
            // Espera, para declarar o jogo como pronto
            yield return new WaitForSeconds(0.2f);
        }
        this.CountPedras();
        Debug.Log("... moção de pedras do campo #" + this.objIdx.ToString() + " para " + GameRules.oases[GameRules.playerTurn - 1].name + " finalizada.");
    }

    public void MoveToCurrPlayerOasis()
    {
        StartCoroutine(this.StartPedrasMoving());
    }

    public bool HasItemsToCollect()
    {
        return !this.isOasis && ((this.objIdx >= 0 && this.objIdx < 6 && GameRules.playerTurn == 2) || (this.objIdx >= 6 && this.objIdx <= 11 && GameRules.playerTurn == 1)) && ((GameRules.phase < 3 && this.pedras.Count > 1 && this.pedras.Count <= 3) || (GameRules.phase == 3 && this.pedras.Count == 3));
    }

    public void SetCounter(int num)
    {
        //this.counter.GetComponent<Text>().text = num.ToString();
        this.counter.text = num.ToString();
    }
    public int GetFieldIdx()
    {
        return this.objIdx;
    }
}
