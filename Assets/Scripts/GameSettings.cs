﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSettings : MonoBehaviour
{
    public AudioSource mainBgTrack;
    public GameObject timerSetter;
    public Toggle enableSoundToggle, enableSFXToggle;
    protected bool bgTrackisPlaying, bgTrackIsEnabled, sfxIsEnabled, isTimerEnabled = false, bgTrackIsPaused = false;
    protected bool isGeneralSettings = false;
    protected string PLAYER_PREF_NAME_TO_TIMER = "_mancbr_timer_secs",
                     PLAYER_PREF_NAME_TO_SFX = "_mancbr_enabled_sfx",
                     PLAYER_PREF_NAME_TO_BGSOUND = "_mancbr_enabled_bg_track";
    // Start is called before the first frame update
    void Start()
    {
    }

    public void ToggleBgTrack(bool state)
    {
        this.bgTrackIsEnabled = !this.bgTrackIsEnabled;
        SetPlayerPrefsAsBool(PLAYER_PREF_NAME_TO_BGSOUND, this.bgTrackIsEnabled);
    }
    public void ToggleSFX(bool state)
    {
        this.sfxIsEnabled = !this.sfxIsEnabled;
        SetPlayerPrefsAsBool(PLAYER_PREF_NAME_TO_SFX, this.sfxIsEnabled);

    }
    public void SetPlayerPrefsAsBool(string playerPrefConst, bool value)
    {
        PlayerPrefs.SetInt(playerPrefConst, (value ? 1 : 0));
    }
    protected void ReloadAudioState()
    {
        bgTrackIsEnabled = this.BgTrackIsEnabled();
        sfxIsEnabled = this.SFXIsEnabled();
        //Debug.Log("Áudio habilitado? " + (this.bgTrackIsEnabled ? "Sim" : "Não") + "\nEfeitos habilitados? " + (this.sfxIsEnabled ? "Sim" : "Não"));
        // Coloca o audio de fundo para tocar, caso esteja habilitado e não esteja tocando
        if (this.bgTrackIsEnabled && !this.bgTrackisPlaying && !this.bgTrackIsPaused)
        {
            this.mainBgTrack.Play();
            this.bgTrackisPlaying = true;
        }
        if (!this.bgTrackIsEnabled && this.bgTrackisPlaying)
        {
            this.mainBgTrack.Stop();
            this.bgTrackisPlaying = false;
        }
    }
    protected void LoadPresetSettings()
    {
        // https://docs.unity3d.com/2019.1/Documentation/ScriptReference/UI.Toggle.html
        this.enableSoundToggle.SetIsOnWithoutNotify(this.BgTrackIsEnabled());
        this.enableSFXToggle.SetIsOnWithoutNotify(this.SFXIsEnabled());
        // Se for uma configuração avançada do jogo ou seja, uma configuração
        // que o jogador não poderá editar no decorrer de uma partida...
        if (this.isGeneralSettings)
        {
            this.timerSetter.SetActive(true);
            //this.enableTimerToggle.SetIsOnWithoutNotify(this.IsTimerEnabled());
        }
        else
        {
            this.timerSetter.SetActive(false);
        }
    }
    protected void PauseBgMusic()
    {
        this.bgTrackIsPaused = !this.bgTrackIsPaused;
        //Debug.Log("Is paused: " + this.bgTrackIsPaused);
        if (this.bgTrackIsPaused && this.bgTrackIsEnabled)
        {
            this.mainBgTrack.Pause();
        }
        else if (!this.bgTrackIsPaused && this.bgTrackIsEnabled)
        {
            this.mainBgTrack.Play();
        }
    }
    protected bool BgTrackIsEnabled()
    {
        return GetPlayerPrefsAsBool(PLAYER_PREF_NAME_TO_BGSOUND);
    }
    protected bool SFXIsEnabled()
    {
        return GetPlayerPrefsAsBool(PLAYER_PREF_NAME_TO_SFX);
    }
    protected int GetGameTimer()
    {
        if (!PlayerPrefs.HasKey(PLAYER_PREF_NAME_TO_TIMER))
            return 0;
        else
            return PlayerPrefs.GetInt(PLAYER_PREF_NAME_TO_TIMER);
    }
    protected bool GetPlayerPrefsAsBool(string playerPrefConst)
    {
        return PlayerPrefs.HasKey(playerPrefConst) && PlayerPrefs.GetInt(playerPrefConst) == 1;
    }
}
