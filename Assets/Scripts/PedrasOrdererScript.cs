﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PedrasOrdererScript : MonoBehaviour
{
    public GameObject examplePedra, gameController/*, pedrasOrdererPanel*/, selectedFieldIndicator, pedrasRepository, selectedPedraIndicator;
    private GameObject selectedPedra, selectedField;
    private GameObject[] placeholders;
    private List<GameObject> pedrasList = new List<GameObject>();
    private Sprite diamond, gold, silver, bronze, ruby;
    private bool alreadyLoaded = false;

    public void SelectPedra(GameObject go)
    {
        // TODO: Terminar a seleção da pedra e a troca de posição entre elas e a classificação das mesmas ao aparecer
        Debug.Log("GO: " + go.name + ", sel. ped.: " + this.selectedPedra + ", " + (this.selectedPedra == go).ToString());
        if (this.selectedPedra == null)
        {
            this.selectedPedra = go;
            selectedPedraIndicator.transform.position = go.transform.position;
            selectedPedraIndicator.SetActive(true);
        }
        else if (this.selectedPedra == go)
        {
            this.selectedPedra = null;
            selectedPedraIndicator.SetActive(false);
        }
        else
        {
            Vector3 befPos = this.selectedPedra.transform.position;
            this.selectedPedra.transform.position = go.transform.position;
            go.transform.position = befPos;
            this.selectedPedra = null;
            selectedPedraIndicator.SetActive(false);
        }
        //if (this.selectedPedra == null)
        //{
        //    this.selectedPedra = 
        //}
    }
    public void ShowPedrasSelectorPanel()
    {
        // workaround para carregamento dos sprites (não funciona como esperado no método 'Start')...
        if (!alreadyLoaded)
        {
            GameController gcs = gameController.GetComponent<GameController>();
            this.diamond = gcs.diamondSprite;
            this.gold = gcs.goldSprite;
            this.silver = gcs.silverSprite;
            this.bronze = gcs.bronzeSprite;
            this.ruby = gcs.rubySprite;
            // Carrega as posições para apacição das pedras
            this.placeholders = new GameObject[this.transform.childCount];
            Debug.Log("Loading placeholders...");
            for (int i = 0; i < this.placeholders.Length; i += 1)
            {
                this.placeholders[i] = this.transform.GetChild(i).gameObject;
                Debug.Log(this.transform.GetChild(i).gameObject.name + " loaded to pos #" + i + " of the placeholder.");
            }
            Debug.Log("Placeholders were loaded.");
            alreadyLoaded = true;
        }
        PrepareAll();
    }
    public void PedrasMovingConfirm()
    {
        //PedrasClassifier pcls = new PedrasClassifier();
        //this.pedrasList.Sort(pcls);
        //Debug.Log("A reordenar pedras...");
        List<GameObject> newPermPedrasList = new List<GameObject>();
        FieldScript selectedField = GameRules.fields[GameRules.selectedFieldIndex].GetComponent<FieldScript>();
        this.ReloadPedras();
        // Inicia o loop entre as pedras reordenadas
        foreach (GameObject p in this.pedrasList)
        {
            //msg += idx.ToString() + " => " + p.GetComponent<PedraDragAndDropScript>().value.ToString() + "; ";
            int pVal = p.GetComponent<CanvasPedraScript>().value;
            List<GameObject> currPedras = selectedField.pedras;
            // Inicia um loop para busca da pedra com o mesmo valor sorteado
            foreach (GameObject cp in currPedras)
            {
                if (pVal == cp.GetComponent<PedraScript>().value)
                {
                    //Debug.Log("... a reordenar pedra #" + pVal.ToString() + "...");
                    newPermPedrasList.Add(cp);
                    currPedras.Remove(cp);
                    break;
                }
            }
        }
        //Debug.Log(msg);
        //Debug.Log("Num. pedras antes: " + selectedField.pedras.Count);
        selectedField.pedras = newPermPedrasList;
        //Debug.Log("Num. pedras depois: " + selectedField.pedras.Count);
        // Desmarca o campo
        selectedFieldIndicator.SetActive(false);
        // fecha a caixa, habilitando a moção das peças
        this._ClosePedrasSelectorPanel(true);
    }
    public void ClosePedrasSelectorPanel()
    {
        this._ClosePedrasSelectorPanel();
    }
    void _ClosePedrasSelectorPanel(bool movePieces = false, bool tooglePedras = true)
    {
        // Desmarca o campo
        selectedFieldIndicator.SetActive(false);
        // Volta a exibir as peças do campo, estas que estarão se movendo...
        if (tooglePedras)
            this.TogglePedras();
        // Mover pedras, se indicado
        if (movePieces)
        {
            this.MovePedras();
        }
        foreach (GameObject p in this.pedrasList)
        {
            Destroy(p);
            //this.pedrasList[idx++] = null;
        }
        this.selectedPedra = null;
        this.selectedPedraIndicator.SetActive(false);
        this.pedrasList.Clear();
        this.transform.parent.gameObject.SetActive(false);
    }
    private void MovePedras()
    {
        GameRules.fields[GameRules.selectedFieldIndex].GetComponent<FieldScript>().SendMessage("StartPedrasDist");
    }
    // PrepareAll prepara a apresentação das pedras para fim de seleção pelo usuário
    void PrepareAll()
    {
        int dCount = 0;
        int rCount = 0;
        int oCount = 0;
        int pCount = 0;
        //int bCount = 0;
        // GameObject do campo selecionado
        GameObject f = GameRules.fields[GameRules.selectedFieldIndex];
        // Instância do script do campo selecionado
        FieldScript selectedField = f.GetComponent<FieldScript>();
        List<GameObject> receivedPedrasList = selectedField.pedras;
        // Cria automaticamente as peças, observando uma distância entre as peças
        foreach (GameObject pedraGameObj in receivedPedrasList)
        {
            string reprPedraName;
            Sprite pedraSprite;
            int pedraValue = pedraGameObj.GetComponent<PedraScript>().value;
            Color colorValueText = Color.black;
            switch (pedraValue)
            {
                case 1000:
                    reprPedraName = "Diamante #" + dCount.ToString();
                    dCount += 1;
                    pedraSprite = this.diamond;
                    colorValueText = Color.black;
                    break;
                case 100:
                    reprPedraName = "Rubi #" + dCount.ToString();
                    rCount += 1;
                    pedraSprite = this.ruby;
                    colorValueText = Color.white;
                    break;
                case 10:
                    reprPedraName = "Ouro #" + oCount.ToString();
                    oCount += 1;
                    pedraSprite = this.gold;
                    break;
                default: // TODO: Ver qual seria o valor do Bronze
                    reprPedraName = "Prata #" + pCount.ToString();
                    pCount += 1;
                    pedraSprite = this.silver;
                    break;
            }
            GameObject pedra = Instantiate(examplePedra);
            pedra.name = reprPedraName;
            pedra.GetComponent<Image>().sprite = pedraSprite;
            RectTransform rt = pedra.GetComponent<RectTransform>();
            pedra.GetComponent<CanvasPedraScript>().value = pedraValue;
            //pedra.GetComponent<PedraDragAndDropScript>().currentIndex = currPos;
            pedra.transform.GetChild(0).GetComponent<Text>().text = pedraValue.ToString();
            pedra.transform.GetChild(0).GetComponent<Text>().color = colorValueText;
            rt.position = new Vector2(80 * ((dCount + rCount + oCount + pCount) - 1), 0);
            //Vector3 rtPosRef = this.placeholders[((dCount + rCount + oCount + pCount) - 1)].transform.position;
            //pedra.transform.position = new Vector2(rtPosRef.x, rtPosRef.y);

            if (pedraValue == 100 || pedraValue == 1000)
            {
                RectTransform rtTxt = pedra.transform.GetChild(0).GetComponent<RectTransform>();
                rt.sizeDelta = new Vector2(rt.sizeDelta.x, rt.sizeDelta.y - 10);
                rtTxt.offsetMin = new Vector2(0, 20);
                //rtTxt.offsetMax = Vector2(0, 20);
            }
            //pedra.GetComponent<RectTransform>().rectTrans = new Rect((rt * (dCount + oCount + pCount)), pedrasExample.transform.position.y, rt.sizeDelta.x, rt.sizeDelta.y - 10);
            //transform.AddChild(pedra);
            pedra.transform.SetParent(pedrasRepository.transform, false);
            pedra.SetActive(true);
            // Adiciona evento para seleção da pedra quando clicada
            pedra.GetComponent<Button>().onClick.AddListener(() => { SelectPedra(pedra); });
            // Adiciona pedra a lista para eventual alteração
            this.pedrasList.Add(pedra);
        }
        //Debug.Log("Num pedras: D(" + dCount.ToString() + "), R(" + rCount.ToString() + "), O(" + oCount.ToString() + ") e P(" + pCount.ToString() + ")");
        // Se todas as pedras forem iguais, apenas os move
        if (dCount == receivedPedrasList.Count || rCount == receivedPedrasList.Count || oCount == receivedPedrasList.Count || pCount == receivedPedrasList.Count)
        {
            _ClosePedrasSelectorPanel(true, false);
            return;
        }
        // Classifica as pedras exibidas conforme o valor delas
        ByMostValuePedrasClassifier2 mostValClassif = new ByMostValuePedrasClassifier2();
        this.pedrasList.Sort(mostValClassif);
        if (GameRules.phase < 3) // na fase 3, em ordem decrescente
            ReloadPedrasByMostValue();
        else
            ReloadPedrasByLessValue();
        // Oculta as pedras no campo de jogo, de modo a não confundir o jogador
        this.TogglePedras();
        // Indica o campo selecionado pelo jogaodr
        selectedFieldIndicator.transform.position = new Vector2(f.transform.position.x, f.transform.position.y);
        selectedFieldIndicator.SetActive(true);
        // Por fim, exibe o painel
        this.transform.parent.gameObject.SetActive(true);
    }
    /**
     * Atualiza a posição das pedras conforme a posição da pedra recebida
     */
    private void ReloadPedras()
    {
        // Re-classifica as pedras exibidas conforme a posição delas
        ByPositionPedrasClassifier2 pcls = new ByPositionPedrasClassifier2();
        this.pedrasList.Sort(pcls);
        for (int n = 0; n < this.pedrasList.Count; n += 1)
        {
            //RectTransform rt = p.GetComponent<RectTransform>();
            //rt.localPosition = new Vector2((55f * idx++), 0);
            this.pedrasList[n].transform.position = this.placeholders[n].transform.position;
        }
    }
    private void ReloadPedrasByMostValue()
    {
        // Re-classifica as pedras exibidas conforme a posição delas
        ByMostValuePedrasClassifier2 clsf = new ByMostValuePedrasClassifier2();
        this.pedrasList.Sort(clsf);
        for (int n = 0; n < this.pedrasList.Count; n += 1)
        {
            //RectTransform rt = p.GetComponent<RectTransform>();
            //rt.localPosition = new Vector2((55f * idx++), 0);
            this.pedrasList[n].transform.position = this.placeholders[n].transform.position;
        }
    }
    private void ReloadPedrasByLessValue()
    {
        // Re-classifica as pedras exibidas conforme a posição delas
        ByMostValuePedrasClassifier2 clsf = new ByMostValuePedrasClassifier2();
        this.pedrasList.Sort(clsf);
        this.pedrasList.Reverse();
        for (int n = 0; n < this.pedrasList.Count; n += 1)
        {
            //RectTransform rt = p.GetComponent<RectTransform>();
            //rt.localPosition = new Vector2((55f * idx++), 0);
            this.pedrasList[n].transform.position = this.placeholders[n].transform.position;
        }
    }

    void TogglePedras()
    {
        foreach (GameObject pgo in GameRules.fields[GameRules.selectedFieldIndex].GetComponent<FieldScript>().pedras)
        {
            pgo.SetActive(!pgo.activeSelf);
        }
    }
}

class ByPositionPedrasClassifier2 : IComparer<GameObject>
{
    public int Compare(GameObject x, GameObject y)
    {
        if (x == null || y == null)
        {
            return 0;
        }
        //Debug.Log("Comparing x(" + x.transform.position.x + ") y(" + y.transform.position.x + ")");
        return x.transform.position.x.CompareTo(y.transform.position.x);
    }
}

class ByMostValuePedrasClassifier2 : IComparer<GameObject>
{
    public int Compare(GameObject x, GameObject y)
    {
        if (x == null || y == null)
        {
            return 0;
        }
        //int xPedraValue = x.GetComponent<Canvas...>().value;
        //int yPedraValue = y.GetComponent<Cancas...>().value;
        return x.GetComponent<CanvasPedraScript>().value.CompareTo(y.GetComponent<CanvasPedraScript>().value);
    }
}