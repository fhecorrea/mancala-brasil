﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : GameSettings
{
    public AudioClip winnersSound, losersSound, crowdApplause;
    public AudioClip[] defaultCoinSFXs = new AudioClip[2],
                        defaultStoneSFXs = new AudioClip[2],
                        stonesOnMetalSFXs = new AudioClip[2],
                        coinsOnMetalSFXs = new AudioClip[2],
                        stonesOnForestSFXs = new AudioClip[2],
                        coinsOnForestSFXs = new AudioClip[2],
                        stonesOnManyCoinsSFXs = new AudioClip[2],
                        coinsOnManyCoinsSFXs = new AudioClip[2],
                        stonesOnManyStonesSFXs = new AudioClip[2],
                        coinsOnManyStonesSFXs = new AudioClip[2],
                        winnersCrowd = new AudioClip[2],
                        losersCrowd = new AudioClip[2],
                        clockTickingSFXs = new AudioClip[2];
    public AudioSource crowd, gameResultSound;
    public Color[] charsMainColor = new Color[4];
    public Image[] infoPlayerPanelPlayersChars = new Image[2];
    public Sprite diamondSprite, goldSprite, silverSprite, rubySprite, bronzeSprite;
    public Sprite[] charactersSprites = new Sprite[4];
    public Text winnerPlayerNameText, winnerPlayerPointsText, winDescText, confirmPanelText, confirmPanelBtnText, gameOverPanelTitleText, opponentNameText, opponentPointsText;
    public Text[] playersAnnouncements = new Text[2];
    public Text[] infoPlayerPanelPlayer1PtsText = new Text[3], infoPlayerPanelPlayer2PtsText = new Text[3], infoPlayerPanelPlayersNames = new Text[2];
    public GameObject p1Panel, p2Panel, attentionMark, gameOverPanel, confirmPanel, pausePanel, settingsPanel, pausePanelMenu, pedraModel, nextRoundBtn, endGameBtn, rankPanel, showRankPanelBtn, recordIndicator, endGameBackToMenuBtn, clockObject, endOfTimeObject, infoPlayerPanel, masterGameBanner;
    public GameObject[] tables, backgrounds;
    public GameObject[] playersGameObjects = new GameObject[2];
    public GameObject[] characters = new GameObject[4];
    private Animator clockAnim;
    private AudioSource clockAudio;
    private bool wasPlayerLoaded = false;
    private const char CONFIRM_TO_EXIT = 'c', CONFIRM_TO_RESTART = 'r';
    private char confirming = '\x20';
    private float t; // time
    private int secondsToPlay = 0;
    private GameObject tmpClockObject, tmpEndOfTimeObject, tmpMasterGameObject;
    private RankingPanelScript rankingPanelScriptInst;
    private Text clockText;
    private Text[] masterGameTexts = new Text[5];
    private Text[][] infoPlayerPanelPlayerPtsText = new Text[2][];
    // Start is called before the first frame update
    void Start()
    {
        GameRules.playerTurn = (Random.Range(0, 10) >= 5 ? 1 : 2);
        GameRules.oases = new GameObject[] { GameObject.Find("oasis_0"), GameObject.Find("oasis_1") };
        this.secondsToPlay = this.GetGameTimer();
        // Mapeia os campos do jogo
        for (int i = 0; i < 12; i += 1)
        {
            GameRules.fields[i] = GameObject.Find("casa_" + i.ToString());
        }
        this.PreparePedras();
        this.LoadScenario();
        this.confirming = '\x20';
        if (GameRules.is2ndPlayerAuto)
        {
            GameRules.playersNames[1] = "CPU";
        }
        //GameRules.selectedCharsIdx (se -1, selecionar automaticamente)
        int[] idxChar = new int[2] { -1, -1 };
        for (int pIdx = 0; pIdx < 2; pIdx += 1)
        {
            GameObject charGo;
            bool flipIsNeeded = false;
            //Vector2 pos = new Vector2(0f, 0f);
            //Quaternion rot = Quaternion.Euler(0f, 0f, 0f);
            int cIdx = GameRules.selectedCharsIdx[pIdx];
            if (cIdx >= 0 && cIdx < 4)
            {
                idxChar[pIdx] = cIdx;
                charGo = this.characters[cIdx];
                if ((pIdx == 0 && cIdx % 2 == 1) || (pIdx == 1 && cIdx % 2 == 0))
                {
                    flipIsNeeded = true;
                }
            }
            else
            {
                // Se não for informado, gera automaticamente
                // (para caso de jogar diretamente do 'Main' scene)
                int randIdx;
                do
                {
                    randIdx = Random.Range(0, 4);
                }
                while (pIdx == 1 && randIdx == idxChar[0]);
                idxChar[pIdx] = randIdx;
                charGo = this.characters[randIdx];
                // Boilerplate...
                if ((pIdx == 0 && randIdx % 2 == 1) || (pIdx == 1 && randIdx % 2 == 0))
                {
                    flipIsNeeded = true;
                }
            }
            // Cores das letras
            this.playersAnnouncements[pIdx].color = charsMainColor[idxChar[pIdx]];
            // GameObject onde ficam os círculos com os personagens escolhidos
            GameObject pGo = Instantiate(charGo, this.playersGameObjects[pIdx].transform);
            pGo.transform.localPosition = new Vector2(0f, 0f);//pos;
            pGo.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
            pGo.GetComponent<SpriteRenderer>().flipX = flipIsNeeded;
            pGo.SetActive(true);
        }
        //this.p1Panel.GetComponent<Image>().color = charsMainColor[idxChar[pIdx]];
        this.rankingPanelScriptInst = this.rankPanel.GetComponent<RankingPanelScript>();
        this.infoPlayerPanelPlayerPtsText[0] = this.infoPlayerPanelPlayer1PtsText;
        this.infoPlayerPanelPlayerPtsText[1] = this.infoPlayerPanelPlayer2PtsText;
        for (int plyr = 0; plyr < 2; plyr += 1)
        {
            this.infoPlayerPanelPlayersChars[plyr].sprite = charactersSprites[idxChar[plyr]];
            this.infoPlayerPanelPlayersNames[plyr].text = GameRules.playersNames[plyr];
            if (((idxChar[plyr] % 2 == 0) && plyr == 1) || ((idxChar[plyr] % 2 != 0) && plyr == 0))
            {
                this.infoPlayerPanelPlayersChars[plyr].gameObject.GetComponent<RectTransform>().rotation = Quaternion.Euler(0f, -180f, 0);
            }
        }
        // Garrega o letreiro indicador de jogada master
        for (int txtIdx = 1; txtIdx < masterGameBanner.transform.childCount; txtIdx += 1)
        {
            this.masterGameTexts[txtIdx - 1] = this.masterGameBanner.transform.GetChild(masterGameBanner.transform.childCount - txtIdx).gameObject.GetComponent<Text>();
        }
        GameRules.isReady = true;
    }
    void SetPlayersPoints()
    {
        for (int plyr = 0; plyr < 2; plyr += 1)
        {
            for (int rnd = 0; rnd < 3; rnd += 1)
            {
                this.infoPlayerPanelPlayerPtsText[plyr][rnd].text = GameRules.pointsPerPhase[plyr, rnd].ToString() + " pts.";
                if (rnd == GameRules.phase)
                {
                    this.infoPlayerPanelPlayerPtsText[plyr][rnd].color = new Color(.95f, .95f, .95f, 1);
                }
                else if (plyr == 1 && GameRules.pointsPerPhase[plyr, rnd] > GameRules.pointsPerPhase[(plyr - 1), rnd])
                {
                    this.infoPlayerPanelPlayerPtsText[plyr][rnd].color = new Color(.5f, 1, .5f, 1);
                    this.infoPlayerPanelPlayerPtsText[(plyr - 1)][rnd].color = new Color(1, .6f, .6f, 1);
                }
                else if (plyr == 1 && GameRules.pointsPerPhase[plyr, rnd] < GameRules.pointsPerPhase[(plyr - 1), rnd])
                {
                    this.infoPlayerPanelPlayerPtsText[(plyr - 1)][rnd].color = new Color(.5f, 1, .5f, 1);
                    this.infoPlayerPanelPlayerPtsText[plyr][rnd].color = new Color(1, .6f, .6f, 1);
                }
            }
        }
    }
    void SetLetterMasterGameText(string text)
    {
        for (int tIdx = 0; tIdx < this.masterGameTexts.Length; tIdx += 1)
        {
            this.masterGameTexts[tIdx].text = text;
        }
        this.tmpMasterGameObject = Instantiate(this.masterGameBanner, this.transform.parent);
        this.tmpMasterGameObject.SetActive(true);
        if (this.SFXIsEnabled())
            this.tmpMasterGameObject.GetComponent<AudioSource>().Play();
    }
    // Update is called once per frame
    void Update()
    {
        ReloadAudioState();
        //Debug.Log("GameRules.isReady: " + GameRules.isReady + "\nGameRules.isOver: " + GameRules.isOver + "\nGameRules.waitingToCollect: " + GameRules.waitingToCollect + "\nGameRules.restToPlayPedras: " + GameRules.restToPlayPedras);
        if (GameRules.isReady && !GameRules.isOver)
        {
            //Debug.Log(GameRules.waitingToCollect && GameRules.restToPlayPedras == 0);
            // Primeiro, verifica se o jogo espera pela coleta das pedras.
            if (GameRules.waitingToCollect && GameRules.restToPlayPedras == 0)
            {
                //StartCoroutine("CollectPedras");
                Debug.Log("CollectPedras executed!");
                CollectPedras();
            }
            // Se não estiver esperando coleta de pedras e algum dos jogadores 
            // tiver mais pontos que teria direito inicialmente ou houver todos
            // os campos indisponíveis para jogo, declara encerrada a fase
            else if (GameRules.emptyFields[0] >= 6 || GameRules.emptyFields[1] >= 6 || GameRules.pointsPerPhase[(GameRules.playerTurn == 1 ? 0 : 1), (GameRules.phase - 1)] > GameRules.pointsPerPlayer)
            {
                Debug.Log("GameRules.emptyFields[0] >= 6" + (GameRules.emptyFields[0] >= 6).ToString() + " (" + GameRules.emptyFields[0].ToString() + ") | GameRules.emptyFields[1] >= 6" + (GameRules.emptyFields[1] >= 6).ToString() + " (" + GameRules.emptyFields[1].ToString() + ") | GameRules.pointsPerPhase[(GameRules.playerTurn == 1 ? 0 : 1), (GameRules.phase - 1)] > GameRules.pointsPerPlayer: (" + GameRules.pointsPerPhase[(GameRules.playerTurn == 1 ? 0 : 1), (GameRules.phase - 1)].ToString() + ") > (" + GameRules.pointsPerPlayer.ToString() + ")");
                EndPhase();
            }
            // Se o jogo estiver pronto para jogo, propõe ao jogador que faça a
            // jogada ou realza o jogo do computador
            else if (GameRules.restToPlayPedras == 0 && !GameRules.waitingToCollect)
            {
                // Anuncia ao jogador da sua vez e ao adversário de que precisa esperar
                if (!this.wasPlayerLoaded)
                {
                    int currPlayerIdx = (GameRules.playerTurn - 1);
                    int waitingPlayerIdx = GameRules.playerTurn == 1 ? 1 : 0;
                    this.playersAnnouncements[currPlayerIdx].text = "Agora é sua vez, " + GameRules.playersNames[currPlayerIdx] + "!";
                    this.playersGameObjects[currPlayerIdx].GetComponent<Animator>().SetTrigger("show");
                    this.playersAnnouncements[waitingPlayerIdx].text = "Aguarde sua vez chegar, " + GameRules.playersNames[waitingPlayerIdx] + "...";
                    this.wasPlayerLoaded = true;
                }
                if (GameRules.playerTurn == 2 && GameRules.is2ndPlayerAuto)
                {
                    //Debug.Log("GameRules.playerTurn: " + GameRules.playerTurn.ToString() + " | GameRules.is2ndPlayerAuto: " + GameRules.is2ndPlayerAuto.ToString());
                    StartCoroutine(PlayForCPU());
                }
                // Habilita o relógio
                this.SetClock();
            }
            this.UpdateClock();
        }
    }

    IEnumerator PlayForCPU()
    {
        // Disposição dos campos conforme a condição deles no momento da escolha
        // (se encontram-se vazios ou não)
        bool[] fieldsByEmptiness = { false, false, false, false, false, false };
        FieldScript field = null;
        // 1# Verifica se há campos que já possuam o limite máx. permitido
        if (GameRules.requiringAttFields[GameRules.playerTurn - 1] > -1)
        {
            //Debug.Log("Jogando por campo com atenção requerida (#" + GameRules.requiringAttFields[GameRules.playerTurn - 1].ToString() + " do P" + GameRules.playerTurn.ToString() + ")");
            field = GameRules.fields[GameRules.requiringAttFields[GameRules.playerTurn - 1]].GetComponent<FieldScript>();
        }
        else// if (GameRules.currentRound > 1.5f) // se esta não for a primeira rodada do player...
        {
            //bool opponentIsVulnerable = false;
            List<int> fields = new List<int> { 6, 7, 8, 9, 10, 11 };
            // Número de pedras do último campo selecionado
            int numPedrasLastField = 0;
            // Último campo selecionado
            int lastFieldIdx = -1;
            // Seleciona aleatoriamente um campo, verifica se ele possui pedras
            // e caso não tenha sido o primeiro campo a ser selecionado, compara
            // este com o último afim de mover o de menor num. de pedras.
            do
            {
                int idx = Random.Range(6, GameRules.fields.Length);
                field = GameRules.fields[idx].GetComponent<FieldScript>();

                if ((GameRules.phase < 3 && field.pedras.Count > 1) || (GameRules.phase == 3 && field.pedras.Count > 2)) // antes era 0 (zero)!
                {
                    // É o primeiro campo com pedras? Se sim, seleciona-o!
                    if (numPedrasLastField == 0 || field.pedras.Count < numPedrasLastField)
                    {
                        lastFieldIdx = idx;
                        numPedrasLastField = field.pedras.Count;
                    }
                }
                else
                {
                    fieldsByEmptiness[(idx - 6)] = true;
                    //field = null;
                }
                // Remove o campo sorteado da lista de campos disponíveis p/ verif.
                fields.Remove(idx);
                //Debug.Log("Apurando F#" + idx + " / field is null: " + (field == null).ToString() + " | lf: " + lastFieldIdx.ToString() + " # p: " + numPedrasLastField.ToString() + " | len fields: " + fields.Count.ToString());
                // Se não houverem mais campos para comparação e o último
                // campo não for o selecionado, então seleciona
                if (fields.Count == 0 && lastFieldIdx != -1 && lastFieldIdx != idx)
                {
                    field = GameRules.fields[idx].GetComponent<FieldScript>();
                }
            } while (fields.Count > 0);
        }
        // Aguarda 2s antes de realizar o movimento
        this.playersAnnouncements[1].text = GameRules.playersNames[1] + " está jogando...";
        yield return new WaitForSeconds(1f);
        //Debug.Log("CPU a distribuir automaticamente pedras da " + field.gameObject.name);
        field.MovePedrasForward();
    }

    void CollectPedras()
    {
        Debug.Log("A iniciar coleta de pedras (CollectPedras)...");
        GameRules.isReady = false;
        StartCoroutine(VerifyFieldsToCollectPedras());
        //yield return new WaitForSeconds(.5f);
        // Verifica o estado dos campos
        //StartCoroutine(VerifyFieldsState());
        Debug.Log("Empty fields [P1: " + GameRules.emptyFields[0].ToString() + " / P2: " + GameRules.emptyFields[1].ToString() + "]");
        Debug.Log("Required attention fields [P1: casa_" + GameRules.requiringAttFields[0].ToString() + " / P2: casa_" + GameRules.requiringAttFields[1].ToString() + "]");
        Debug.Log("Rodada " + System.Math.Ceiling(GameRules.currentRound).ToString() + " termnada! Pontos a disputar: " + GameRules.toDisputePoints[0] + " (P1)" + GameRules.toDisputePoints[1] + " (P2)");
        // Notifica o jogador que a jogada já foi realizada.
        this.playersAnnouncements[(GameRules.playerTurn - 1)].text = "... pronto!";
        //StartCoroutine(CountdownToReopen());
    }
    IEnumerator VerifyFieldsToCollectPedras()
    {
        // Notifica o jogador de que ainda vai ter a coleta
        this.playersAnnouncements[(GameRules.playerTurn - 1)].text = "... " + GameRules.playersNames[(GameRules.playerTurn - 1)] + " está coletando pedras...";
        List<FieldScript> seqFieldsList = new List<FieldScript>();
        int seqNum = 0;
        int lastNum = 0;
        Debug.Log("Waiting to collect...");
        yield return new WaitForSeconds(1f + (.5f * GameRules.restToPlayPedras));
        Debug.Log("... ok. Starting foreach loopin");
        foreach (int fIdx in GameRules.roundFieldIndex)
        {
            FieldScript fs = GameRules.fields[fIdx].GetComponent<FieldScript>();
            // Se houver itens para coleta e já houver campos na sequência e
            // o número de pedras for igual ao do último campo adicionado (fase 2 e 3) a 
            // sequencia ou for o primeiro item da sequência, acrescenta a lista
            Debug.Log("Campo \"" + GameRules.fields[fIdx].name + "\" = " + fs.pedras.Count.ToString() + " / aggr. (" + GameRules.restToPlayPedras.ToString() + ") seq. len. (" + seqFieldsList.Count.ToString() + ") hasItms2Col? " + fs.HasItemsToCollect() + "is a seq? " + ((GameRules.phase == 3 && seqFieldsList.Count > 0 && seqFieldsList[(seqFieldsList.Count - 1)].pedras.Count == fs.pedras.Count) || (seqFieldsList.Count > 0 && GameRules.phase < 3) || seqFieldsList.Count == 0).ToString());
            if (fs.HasItemsToCollect() && ((GameRules.phase == 3 && seqFieldsList.Count > 0 && seqFieldsList[(seqFieldsList.Count - 1)].pedras.Count == fs.pedras.Count) || (seqFieldsList.Count > 0 && GameRules.phase < 3) || seqFieldsList.Count == 0))
            {
                // Faz uma pilha com os campos
                seqFieldsList.Add(fs);
                seqNum += 1;
                GameRules.restToPlayPedras += fs.pedras.Count;
                // Para fins de amostrador de jogada master
                if (seqFieldsList.Count > 0)
                {
                    if (lastNum != 0 && seqFieldsList[(seqFieldsList.Count - 1)].pedras.Count != lastNum)
                    {
                        lastNum = 0;
                    }
                    else if (GameRules.phase < 3) // na fase 3, seria sempre "3x? Master"...
                    {
                        lastNum = seqFieldsList[(seqFieldsList.Count - 1)].pedras.Count;
                    }
                }
            }
            else
            {
                /* TODO: Coleta apenas no último ou coleta a última sequência?
                if (seqFieldsList.Count > 1)
                {
                    seqFieldsList = new List<FieldScript>() { seqFieldsList[(seqFieldsList.Count - 1)] };
                    seqNum = 1;
                }
                seqNum = seqFieldsList.Count;
                */
                seqFieldsList.Clear();
                seqNum = 0;
                GameRules.restToPlayPedras = 0;
                lastNum = 0;
            }
            // Marca o campo, caso ele esteja cheio
            if (fs.pedras.Count >= GameRules.MAX_PEDRAS_PER_FIELD)
            {
                Transform fieldTransform = GameObject.Find("field_" + fIdx.ToString()).transform;
                if (fieldTransform.Find("attention_to_" + GameRules.fields[fIdx].name) == null)
                {
                    GameObject attentionToField = Instantiate(this.attentionMark, fieldTransform, false);
                    attentionToField.transform.position = new Vector2(fieldTransform.position.x + .3f, fieldTransform.position.y - 0.1f);
                    attentionToField.name = "attention_to_" + GameRules.fields[fIdx].name;
                    attentionToField.SetActive(true);
                    Debug.Log("Campo " + GameRules.fields[fIdx].name + " requer atenção!");
                }
            }
        }
        Debug.Log("Ao fim da verificação: ln(" + lastNum.ToString() + "), seq(" + seqNum.ToString() + ") aggr. (" + GameRules.restToPlayPedras.ToString() + ") seq. len. (" + seqFieldsList.Count.ToString() + ")");
        GenMasterGameText(lastNum, seqNum);
        StartCoroutine(this.CollectFromFieldsList(seqFieldsList));
        Debug.Log("Pedras collected from " + seqFieldsList.Count.ToString() + " fields.");
        GameRules.roundFieldIndex.Clear();
        // Verifica o estado dos campos
        StartCoroutine(VerifyFieldsState());
    }
    IEnumerator CollectFromFieldsList(List<FieldScript> fl)
    {
        if (fl.Count > 0)
        {
            foreach (FieldScript f in fl)
            {
                f.SendMessage("StartPedrasMoving");
            }
            yield return new WaitForSeconds(.5f);
        }
    }
    IEnumerator VerifyFieldsState()
    {
        for (int p = 0; p < 2; p += 1)
        {
            GameRules.requiringAttFields[p] = -1;
            GameRules.emptyFields[p] = 0;
            GameRules.toDisputePoints[p] = 0;
        }
        // Pausa para dar tempo das animações terminarem, caso haja algum ocorrendo
        yield return new WaitForSeconds(.5f);
        // Verifica campo por campo se há algum campo vazio, cheio ou quantos pontos ainda faltam disputar
        for (int fIdx2 = 0; fIdx2 < GameRules.fields.Length; fIdx2 += 1)
        {
            FieldScript field = GameRules.fields[fIdx2].GetComponent<FieldScript>();
            // cheios
            if (field.pedras.Count >= GameRules.MAX_PEDRAS_PER_FIELD)
            {
                GameRules.requiringAttFields[(fIdx2 < 6 ? 0 : 1)] = fIdx2;
            }
            // campos indisponíveis para jogo...
            else if ((GameRules.phase < 3 && field.pedras.Count <= 1) || (GameRules.phase == 3 && field.pedras.Count <= 2))
            {
                GameRules.emptyFields[(fIdx2 < 6 ? 0 : 1)] += 1;
            }
            // Verifica se os valores dos campos são insuficientes para o jogador 
            // em desvantagem virar. Se for, decreta vitória matemática ao player na frente
            // TODO: Melhorar essa contagem
            else
            {
                foreach (GameObject p in field.pedras)
                {
                    GameRules.toDisputePoints[(fIdx2 >= 6 ? 1 : 0)] += p.GetComponent<PedraScript>().value;
                }
            }
        }
        StartCoroutine(CountdownToReopen());
    }
    IEnumerator CountdownToReopen()
    {
        yield return new WaitForSeconds(2f + (.5f * GameRules.restToPlayPedras));
        this.DestroyTmpClockObject();
        // Desmarca espera por coleta dos campos
        GameRules.waitingToCollect = false;
        this.PassToNextPlayerTurn();
    }
    void GenMasterGameText(int lastNum, int seq)
    {
        string t = "";
        if (seq == 0)
        {
            return;
        }
        if (lastNum > 0)
        {
            t += lastNum.ToString();
        }
        t += "x" + seq.ToString() + " Master" + (seq > 1 ? (seq > 2 ? "!!!" : "!!") : "!");
        SetLetterMasterGameText(t);
    }

    public void ToggleConfirmPanel()
    {
        this.confirmPanel.SetActive(!this.confirmPanel.activeSelf);
        if (this.confirmPanel.activeSelf)
            this.confirmPanelText.text = "Ao \"" + this.confirmPanelBtnText.text + "\", " + (GameRules.is2ndPlayerAuto ? "o jogador perderá" : "os jogadores perderão") + " todo o progresso obtido na partida.";
    }
    public void ConfirmExitFromGame()
    {
        this.confirming = CONFIRM_TO_EXIT;
        this.confirmPanelBtnText.text = "Sair";
        this.ToggleConfirmPanel();
    }
    public void ConfirmGameRestart()
    {
        this.confirming = CONFIRM_TO_RESTART;
        this.confirmPanelBtnText.text = "Reiniciar partida";
        this.ToggleConfirmPanel();
    }
    public void ConfirmFromPanel()
    {
        ToggleConfirmPanel();
        switch (this.confirming)
        {
            case CONFIRM_TO_EXIT:
                this.BackToMenu();
                break;
            case CONFIRM_TO_RESTART:
                if (pausePanel.activeSelf)
                {
                    pausePanel.SetActive(false);
                    infoPlayerPanel.SetActive(false);
                }
                this.RestartGame();
                break;
            default:
                break;
        }
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
    public void TogglePause()
    {

        if (Time.timeScale == 1.0f)
        {
            this.PauseBgMusic();
            Time.timeScale = 0f;
            pausePanel.SetActive(true);
            SetPlayersPoints();
        }
        else
        {
            this.PauseBgMusic();
            Time.timeScale = 1f;
            pausePanel.SetActive(false);
        }
        this.infoPlayerPanel.SetActive(this.pausePanel.activeSelf);
    }
    void ClearReqAttFields()
    {
        for (int p = 0; p < 2; p += 1)
        {
            // Limpa os indicadores de campos que necessitam de atenção pré-existentes
            if (GameRules.requiringAttFields[p] > -1)
            {
                Transform attentionMark = GameObject.Find("field_" + GameRules.requiringAttFields[p]).transform.Find("attention_to_" + GameRules.fields[GameRules.requiringAttFields[p]].name);
                Destroy(attentionMark.gameObject);
                GameRules.requiringAttFields[p] = -1;
            }
        }
    }
    public void RestartGame()
    {
        GameRules.isReady = GameRules.isOver = false;
        //SceneManager.LoadScene("Main");
        GameRules.playerTurn = (Random.Range(0, 10) >= 5 ? 1 : 2);
        GameRules.phase = 1;
        this.PreparePedras();
        GameRules.currentRound = 1f;
        GameRules.roundFieldIndex.Clear();
        this.ClearReqAttFields();
        GameRules.emptyFields[0] = GameRules.emptyFields[1] = 0;
        //GameRules.toDisputePoints[0] = GameRules.toDisputePoints[1] = 672;
        GameRules.pointsPerPhase = new int[2, 3];
        gameOverPanel.SetActive(false);
        this.confirming = '\x20';
        this.LoadScenario();
        if (infoPlayerPanel.activeSelf)
            infoPlayerPanel.SetActive(false);
        GameRules.isReady = true;
    }
    public void EndPhase()
    {
        int winnerIdx = -1;
        // Em caso do jogo ser encerrado antes da passagem de vez entre os jogadores...
        if (this.wasPlayerLoaded)
        {
            this.playersGameObjects[(GameRules.playerTurn - 1)].GetComponent<Animator>().SetTrigger("hide");
            this.wasPlayerLoaded = false;
        }
        GameRules.isOver = true;
        this.recordIndicator.SetActive(false);
        gameOverPanelTitleText.text = "Fim da " + GameRules.phase.ToString() + "ª fase!";
        if (GameRules.pointsPerPhase[0, (GameRules.phase - 1)] == GameRules.pointsPerPhase[1, (GameRules.phase - 1)])
        {
            crowd.clip = crowdApplause;
            winnerPlayerNameText.text = "Ninguém ganhou nesta fase!";
            winDescText.text = "A fase terminou em empate...";
            winnerPlayerPointsText.text = "Ambos fizeram " + GameRules.pointsPerPhase[0, (GameRules.phase - 1)].ToString() + " pts.";
        }
        else
        {
            if (GameRules.pointsPerPhase[0, (GameRules.phase - 1)] > GameRules.pointsPerPhase[1, (GameRules.phase - 1)])
            {
                winnerIdx = 0;
                winnerPlayerNameText.text = GameRules.playersNames[winnerIdx] + " venceu esta " + GameRules.phase.ToString() + "ª fase!";
                winnerPlayerPointsText.text = GameRules.pointsPerPhase[0, (GameRules.phase - 1)].ToString() + " pts.";
                // Som de vitória do primeiro jogador
                crowd.clip = winnersCrowd[(int)System.Math.Floor(Random.Range(0f, 1.99f))];
                gameResultSound.clip = winnersSound;
            }
            else if (GameRules.pointsPerPhase[0, (GameRules.phase - 1)] < GameRules.pointsPerPhase[1, (GameRules.phase - 1)])
            {
                winnerIdx = 1;
                if (GameRules.is2ndPlayerAuto)
                {
                    winnerPlayerNameText.text = "O computador ganhou!";
                    // Som de derrota do jogador
                    crowd.clip = losersCrowd[(int)System.Math.Floor(Random.Range(0f, 1.99f))];
                    gameResultSound.clip = losersSound;
                }
                else
                {
                    winnerPlayerNameText.text = GameRules.playersNames[winnerIdx] + " venceu esta " + GameRules.phase.ToString() + "ª fase";
                    // O som para a vitória do segundo jogador é o mesmo do primeiro,
                    // a fim de não-desprestigiar o segundo jogador.
                    // Em caso de multiplayer, TODO: Dar mesmo tratamento do CPU
                    crowd.clip = winnersCrowd[(int)System.Math.Floor(Random.Range(0f, 1.99f))];
                    gameResultSound.clip = winnersSound;
                }
                winnerPlayerPointsText.text = GameRules.pointsPerPhase[1, (GameRules.phase - 1)].ToString() + " pts.";
            }

            // Amostra da pontuação e nome do jogador derrotado
            opponentNameText.text = "do(a) " + GameRules.playersNames[(winnerIdx == 0 ? 1 : 0)] + "...";
            opponentPointsText.text = GameRules.pointsPerPhase[(winnerIdx == 0 ? 1 : 0), (GameRules.phase - 1)].ToString() + " pts.";
            opponentPointsText.gameObject.transform.parent.gameObject.SetActive(true);

            Debug.Log("emptyFields[]: " + GameRules.emptyFields.Length.ToString() + ", toDisputePoints[]: " + GameRules.toDisputePoints.Length.ToString());
            int greatPoint = GameRules.pointsPerPlayer + (int)System.Math.Ceiling((GameRules.pointsPerPlayer * 0.10));
            // Se a pontuação do jogador for 10% maior do que o que ele poderia ganhar normalmente,
            // exibe uma mensagem 'humilhante' ao perdedor. Caso contrário, exibe a mensagem normal.
            if (GameRules.toDisputePoints[winnerIdx] >= greatPoint)
            {
                Debug.Log("Pontuação do vencedor maior que " + GameRules.toDisputePoints[winnerIdx].ToString() + " (" + greatPoint.ToString() + ")");
                winDescText.text = "Vitória acachapante... Nem que seu adversário fizesse os " + GameRules.toDisputePoints[winnerIdx].ToString() + " pontos restantes!";
            }
            else
            {
                winDescText.text = "Vitória por maior número de pontos...";
            }
            Debug.Log("Fim da rodada, '" + GameRules.playersNames[winnerIdx] + "' foi o ganhador com " + GameRules.pointsPerPhase[winnerIdx, (GameRules.phase - 1)].ToString() + "!");
        }
        SetPlayersPoints();
        gameOverPanel.SetActive(true);
        infoPlayerPanel.SetActive(true);
        showRankPanelBtn.SetActive(false);
        if (GameRules.phase >= 3)
        {
            endGameBackToMenuBtn.SetActive(true);
            nextRoundBtn.SetActive(false);
            endGameBtn.SetActive(true);
        }
        else
        {
            endGameBackToMenuBtn.SetActive(false);
            nextRoundBtn.SetActive(true);
            endGameBtn.SetActive(false);
        }
        // Toca a música do ganhador
        this.crowd.Play();
        this.gameResultSound.Play();
    }
    public void EndGame()
    {
        int winnerIdx = -1;
        gameOverPanel.SetActive(false); // apenas para dar um efeito de carregamento de outra tela
        gameOverPanelTitleText.text = "Fim de Jogo!";
        int totalP1Points = GameRules.pointsPerPhase[0, 0] + GameRules.pointsPerPhase[0, 1] + GameRules.pointsPerPhase[0, 2];
        int totalP2Points = GameRules.pointsPerPhase[1, 0] + GameRules.pointsPerPhase[1, 1] + GameRules.pointsPerPhase[1, 2];
        if (totalP1Points == totalP2Points)
        {
            winnerPlayerNameText.text = "Ninguém ganhou!";
            winDescText.text = "Empate por pontos...";
            winnerPlayerPointsText.text = "Ambos com " + totalP1Points.ToString() + " pts.";
            crowd.clip = crowdApplause;
        }
        else
        {
            int posRank = -1;
            if (totalP1Points > totalP2Points)
            {
                winnerIdx = 0;
                winnerPlayerNameText.text = GameRules.playersNames[winnerIdx] + " venceu a partida!";
                winnerPlayerPointsText.text = totalP1Points.ToString() + "pts.";
                posRank = this.rankingPanelScriptInst.HasRankLevel(totalP1Points);
                crowd.clip = winnersCrowd[(int)System.Math.Floor(Random.Range(0f, 1.99f))];
                gameResultSound.clip = winnersSound;
            }
            else if (totalP1Points < totalP2Points)
            {
                winnerIdx = 1;
                if (GameRules.is2ndPlayerAuto)
                {
                    winnerPlayerNameText.text = "O computador ganhou!";
                    crowd.clip = losersCrowd[(int)System.Math.Floor(Random.Range(0f, 1.99f))];
                    gameResultSound.clip = losersSound;
                }
                else
                {
                    winnerPlayerNameText.text = GameRules.playersNames[winnerIdx] + " venceu a partida!";
                    crowd.clip = winnersCrowd[(int)System.Math.Floor(Random.Range(0f, 1.99f))];
                    gameResultSound.clip = winnersSound;
                    posRank = this.rankingPanelScriptInst.HasRankLevel(totalP2Points);
                }
                winnerPlayerPointsText.text = totalP2Points.ToString() + " pts.";
            }

            if (posRank > 0)
            {
                this.recordIndicator.SetActive(true);
                this.rankingPanelScriptInst.RegisterRank(posRank, GameRules.playersNames[winnerIdx], (GameRules.pointsPerPhase[winnerIdx, 0] + GameRules.pointsPerPhase[winnerIdx, 1] + GameRules.pointsPerPhase[winnerIdx, 2]));
            }

            // Amostra da pontuação e nome do jogador derrotado
            opponentNameText.text = "do(a) " + GameRules.playersNames[(winnerIdx == 0 ? 1 : 0)] + "...";
            opponentPointsText.text = (GameRules.pointsPerPhase[(winnerIdx == 0 ? 1 : 0), 0] + GameRules.pointsPerPhase[(winnerIdx == 0 ? 1 : 0), 1] + GameRules.pointsPerPhase[(winnerIdx == 0 ? 1 : 0), 2]).ToString() + " pts.";
            opponentPointsText.gameObject.transform.parent.gameObject.SetActive(true);
            /*
            int greatPoint = GameRules.pointsPerPlayer + (int)System.Math.Ceiling((GameRules.pointsPerPlayer * 0.50));
            // Se a pontuação do jogador for 10% maior do que o que ele poderia ganhar normalmente,
            // exibe uma mensagem 'humilhante' ao perdedor. Caso contrário, exibe a mensagem normal.
            if (GameRules.toDisputePoints[winnerIdx] >= greatPoint)
            {
                Debug.Log("Pontuação do vencedor maior que " + GameRules.toDisputePoints[winnerIdx].ToString() + " (" + greatPoint.ToString() + ")");
                winDescText.text = "Vitória acachapante... Nem que seu adversário fizesse os " + GameRules.toDisputePoints[winnerIdx].ToString() + " pontos restantes!";
            }
            else
            {*/
            winDescText.text = "Vitória por maior número de pontos...";
            //}
            Debug.Log("Game Over, '" + GameRules.playersNames[winnerIdx] + "' foi o ganhador com " + (GameRules.pointsPerPhase[winnerIdx, 0] + GameRules.pointsPerPhase[winnerIdx, 1] + GameRules.pointsPerPhase[winnerIdx, 2]).ToString() + "!");
        }
        gameOverPanel.SetActive(true);
        infoPlayerPanel.SetActive(true);
        nextRoundBtn.SetActive(false);
        endGameBtn.SetActive(false);
        endGameBackToMenuBtn.SetActive(true);
        showRankPanelBtn.SetActive(true);
        // Toca a música do ganhador
        this.crowd.Play();
        this.gameResultSound.Play();
    }
    public void GoToTheNextRound()
    {
        this.ToggleCurrentPhase();
        GameRules.isReady = GameRules.isOver = false;
        GameRules.phase = GameRules.phase + 1;
        this.PreparePedras();
        GameRules.playerTurn = GameRules.playerTurn == 1 ? 2 : 1;
        /* INICIO BOLIERPLATE */
        GameRules.currentRound = 1f;
        GameRules.roundFieldIndex.Clear();
        this.ClearReqAttFields();
        GameRules.emptyFields[0] = GameRules.emptyFields[1] = 0;
        //GameRules.toDisputePoints[0] = GameRules.toDisputePoints[1] = 672;
        gameOverPanel.SetActive(false);
        this.confirming = '\x20';
        if (infoPlayerPanel.activeSelf)
            infoPlayerPanel.SetActive(false);
        /* fim boilerplate */
        this.ToggleCurrentPhase();
        this.LoadScenario();
        GameRules.isReady = true;
    }
    void PassToNextPlayerTurn()
    {
        // Passa a vez ao jogador adversário
        if (this.wasPlayerLoaded)
        {
            this.playersGameObjects[(GameRules.playerTurn - 1)].GetComponent<Animator>().SetTrigger("hide");
            this.wasPlayerLoaded = false;
        }
        GameRules.playerTurn = GameRules.playerTurn == 1 ? 2 : 1;
        // Avança em mais um na jogada e rodada de jogo
        GameRules.currentRound += .5f;
        // Re-habilita estado do jogo para estar pronto para jogo do jogador
        GameRules.isReady = true;
        Debug.Log("Agora é a vez do P" + GameRules.playerTurn.ToString());
    }
    /*
     * Retorna o número de peças por campo de jogo de acordo com a fase do jogo
     */
    int getIniPiecesPerField()
    {
        if (GameRules.phase == 1)
        {
            return 4;
        }
        //else if (GameRules.phase == 2)
        //{
        //    return 5;
        //}
        else
        {
            return 5;//6;
        }
    }
    /**
     * Retorna o valor e imagem de uma pedra conforme a fase do jogo
     */
    int getPedraData(int idx, SpriteRenderer sprRndr)
    {
        if (GameRules.phase > 1 && idx == 0) // A 1a peça das fases superiores
        {
            sprRndr.sprite = diamondSprite;
            sprRndr.color = new Color(1f, 1f, 1f, 0.8f);
            return 1000;
        }
        // A primeira da primeira fase ou a segunda das demais fases
        else if ((idx == 0 && GameRules.phase == 1) || (GameRules.phase == 2 && idx == 1) || (GameRules.phase == 3 && (idx >= 1 && idx < 4)))
        {
            sprRndr.sprite = rubySprite;
            sprRndr.color = new Color(1f, 1f, 1f, 0.8f);
            return 100;
        }
        else if ((GameRules.phase == 1 && (idx == 1 /*|| idx == 2*/)) || (GameRules.phase == 2 && idx == 2) || (GameRules.phase == 3 && idx > 3))
        {
            sprRndr.sprite = goldSprite;
            return 10;
        }
        else
        {
            sprRndr.sprite = silverSprite;
            return 1;
        }
    }
    /**
     * Prepara e dispõe as pedras em seus respectivos campos no tabuleiro de jogo
     */
    private void PreparePedras()
    {
        string tmpPedrasListName = "tmp_pedras_list";
        // Se o objeto que armazena as pedras criadas já existir, recria-o
        // TODO: Apenas destruir pedras existentes, se for o caso
        GameObject pedrasList = GameObject.Find(tmpPedrasListName);
        if (pedrasList != null)
        {
            Destroy(pedrasList);
        }
        pedrasList = new GameObject();
        pedrasList.name = tmpPedrasListName;
        pedrasList.SetActive(true);

        int fInternalPedraIdx;
        int idx = 0;

        GameRules.pointsPerPlayer = 0;
        GameRules.toDisputePoints[0] = 0;
        GameRules.toDisputePoints[1] = 0;

        for (int fIdx = 0; fIdx < GameRules.fields.Length; fIdx += 1)
        {
            GameObject field = GameRules.fields[fIdx];
            FieldScript fScrpt = field.GetComponent<FieldScript>();
            fScrpt.pedras.Clear(); // em caso de reinício de jogo...
            fInternalPedraIdx = 0;
            for (int pIdx = 0; pIdx < this.getIniPiecesPerField(); pIdx += 1)
            {
                GameObject pedra = Instantiate(pedraModel);
                pedra.name = "p_" + idx.ToString();
                int pval = this.getPedraData(fInternalPedraIdx, pedra.GetComponent<SpriteRenderer>());
                // Atribui valor a pedra e determina a quantidade inicial de pontos em disputa
                pedra.GetComponent<PedraScript>().value = pval;
                GameRules.toDisputePoints[(fIdx >= 6 ? 1 : 0)] += pval;
                // Atribui o som das peças conforme o tipo delas (se moeda ou pedra)
                if (pval > 10)
                {
                    pedra.GetComponent<PedraScript>().SetDefaultSFXs(this.defaultStoneSFXs);
                    pedra.GetComponent<PedraScript>().SetMetalSFXs(this.stonesOnMetalSFXs);
                    pedra.GetComponent<PedraScript>().SetForestSFXs(this.stonesOnForestSFXs);
                    pedra.GetComponent<PedraScript>().SetOnManyCoinsSFXs(this.stonesOnManyCoinsSFXs);
                    pedra.GetComponent<PedraScript>().SetOnManyStonesSFXs(this.stonesOnManyStonesSFXs);
                }
                else
                {
                    pedra.GetComponent<PedraScript>().SetDefaultSFXs(this.defaultCoinSFXs);
                    pedra.GetComponent<PedraScript>().SetMetalSFXs(this.coinsOnMetalSFXs);
                    pedra.GetComponent<PedraScript>().SetForestSFXs(this.coinsOnForestSFXs);
                    pedra.GetComponent<PedraScript>().SetOnManyCoinsSFXs(this.coinsOnManyCoinsSFXs);
                    pedra.GetComponent<PedraScript>().SetOnManyStonesSFXs(this.coinsOnManyStonesSFXs);
                }
                // "Sorteia" uma posição para a pedra no tabuleiro de forma fazer parecer mais espontânea
                float destX = Random.Range(field.transform.position.x - .5f, field.transform.position.x + .5f);
                float destY = Random.Range(field.transform.position.y - .2f, field.transform.position.y + .4f);
                pedra.transform.position = new Vector2(destX, destY);
                fScrpt.pedras.Add(pedra); //pedras[pIdx] = pedra;
                fInternalPedraIdx += 1;
                idx += 1;
                pedra.transform.SetParent(pedrasList.transform);
            }
            // Contabiliza as pedras existentes no campo e atualiza o contador
            fScrpt.SetCounter(fInternalPedraIdx);
        }

        GameRules.oases[0].GetComponent<FieldScript>().pedras.Clear();
        GameRules.oases[0].GetComponent<FieldScript>().SetCounter(0);
        GameRules.oases[1].GetComponent<FieldScript>().pedras.Clear();
        GameRules.oases[1].GetComponent<FieldScript>().SetCounter(0);
        // Se este método estiver sendo executado pela primeira vez na partida,
        // atualiza o número de pontos por jogador no início da partida.
        if (GameRules.pointsPerPlayer == 0)
        {
            GameRules.pointsPerPlayer = GameRules.toDisputePoints[0];
        }
        Debug.Log("Bem vindo ao Mancala Brasil! Pontos a disputar: " + GameRules.toDisputePoints[0] + " (P1)" + GameRules.toDisputePoints[1] + " (P2) | " + GameRules.pointsPerPlayer.ToString() + " (PPP)");
    }
    private void LoadScenario()
    {
        for (int r = 0; r < 3; r += 1)
        {
            this.tables[r].SetActive(false);
            this.backgrounds[r].SetActive(false);
        }
        this.tables[(GameRules.phase - 1)].SetActive(true);
        this.backgrounds[(GameRules.phase - 1)].SetActive(true);
    }
    private void SetClock()
    {
        if (this.secondsToPlay > 0 && this.tmpClockObject == null && GameRules.isReady && !GameRules.isOver)
        {
            float posX = -9f, posY = -4f;
            if (GameRules.playerTurn == 1)
            {
                posX *= (-1);
                posY *= (-1);
            }
            this.tmpClockObject = Instantiate(clockObject, this.gameObject.transform, false);
            this.tmpClockObject.SetActive(true);
            this.tmpClockObject.transform.position = new Vector3(posX, posY, 0f);
            this.clockText = this.tmpClockObject.transform.GetChild(2).GetComponent<Text>();
            this.clockAnim = this.tmpClockObject.GetComponent<Animator>();
            this.t = 0f;
            this.clockAudio = this.tmpClockObject.GetComponent<AudioSource>();
            this.clockAudio.clip = this.clockTickingSFXs[0];
            this.clockAudio.Play();

        }
    }
    private void UpdateClock()
    {
        // Se o contador estiver habilitado e existir um contador criado no 
        // cenário e este estiver ativo, e o jogo não estiver pausado
        if (this.secondsToPlay > 0 && this.tmpClockObject != null && this.tmpClockObject.activeSelf && this.clockText != null && !this.pausePanel.activeSelf)
        {
            this.t += .015f;
            int remainingTime = (this.secondsToPlay - (int)System.Math.Floor(this.t));
            if (remainingTime >= 0)
            {
                this.clockText.text = remainingTime.ToString();
                this.clockAnim.SetInteger("remain_secs", remainingTime);
                AudioSource clkAudio = this.tmpClockObject.GetComponent<AudioSource>();
                // se o som do cron. estiver normal e o tempo restante for 1/3 do tempo original,
                // passa a tocar aquele som de cronômetro apressado
                if (remainingTime < (int)System.Math.Ceiling((float)(this.secondsToPlay / 3)) && this.clockAudio.clip != this.clockTickingSFXs[1])
                {
                    this.clockAudio.clip = this.clockTickingSFXs[1];
                    this.clockAudio.Play();
                }
            }
            else
            {
                this.DestroyTmpClockObject();
                this.tmpEndOfTimeObject = Instantiate(this.endOfTimeObject, this.gameObject.transform);
                this.tmpEndOfTimeObject.SetActive(true);
                StartCoroutine(DestroyTmpEndOfTimeObject());
            }
        }
    }
    private void DestroyTmpClockObject()
    {
        if (this.secondsToPlay > 0 && this.tmpClockObject != null && this.tmpClockObject.activeSelf && this.clockText != null)
        {
            GameRules.isReady = false;
            Destroy(this.tmpClockObject);
            this.clockText = null;
            this.clockAnim = null;
            this.clockAudio = null;
        }
    }
    private IEnumerator DestroyTmpEndOfTimeObject()
    {
        yield return new WaitForSeconds(2f);
        Destroy(this.tmpEndOfTimeObject);
        this.PassToNextPlayerTurn();
    }
    public void ToggleSettingsPanel()
    {
        this.settingsPanel.SetActive(!this.settingsPanel.activeSelf);
        this.pausePanelMenu.SetActive(!this.pausePanelMenu.activeSelf);
        this.infoPlayerPanel.SetActive(this.pausePanelMenu.activeSelf);
        if (this.settingsPanel.activeSelf)
            this.LoadPresetSettings();
    }

    public void ToggleCurrentPhase()
    {
        this.tables[(GameRules.phase - 1)].SetActive(this.tables[(GameRules.phase - 1)].activeSelf);
        this.backgrounds[(GameRules.phase - 1)].SetActive(this.backgrounds[(GameRules.phase - 1)].activeSelf);
    }
}
